let a = prompt("Nhập vào một từ");
if (a.toLowerCase() === a.split('').reverse().join('').toLowerCase()) {
    document.write('Chuỗi ' + '"' + a + '"' + ' là palindrome.');
} else {
    document.write('Chuỗi ' + '"' + a + '"' + ' không phải là palindrome.');
}