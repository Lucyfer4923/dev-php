#include<iostream>
#include<math.h>
using namespace std;

int main() {
    // nhập vào 1 mảng gồm n phần tử
    int n;
    cin >> n;
    int a[n];
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    // sắp xếp các phần tử trong mảng theo thứ tự tăng dần rồi in ra màn hình
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (a[i] > a[j]) {
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
    for (int i = 0; i < n; i++) {
        cout << a[i] << " ";
    }
    return 0;
    
}